#include "exalt.h"
#include "draw.h"
#include <cassert>

namespace exalt {

object::object() {}

object::~object() {}

void object::step() {}

void object::draw() const {}

void object::update() {
	if (active) { step(); }
	if (visible) { draw(); }
}

objectdrawable::objectdrawable() {}

objectdrawable::objectdrawable(int xx, int yy, int ww, int hh, SDL_Texture* ss) {
	x=xx;
	y=yy;
	w=ww;
	h=hh;
	sprite=ss;
}

objectdrawable::~objectdrawable() {}

void objectdrawable::step() {}

void objectdrawable::update() {
	if (active) { step(); }
	if (visible) { draw(); }
}

void objectdrawable::draw() const {
	DrawSprite(exalt::engine::renderer, sprite, area(x,y,w,h));
}
}