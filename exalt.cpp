#define EXALT_EXCEPTIONS
//	Enable catching exceptions in main runloop. If enabled, a popup describing
//	the exception is shown when one is encountered. Otherwise the engine will
//	simply crash. This may slightly slow down execution.

#ifndef EXALT_MIXER_CHANNELS
#define EXALT_MIXER_CHANNELS 16
#endif // !EXALT_MIXER_CHANNELS


#include "exalt.h"
#include "SDL_mixer.h"
#include "SDL_ttf.h"
#include <windows.h>
#include <iostream>
namespace exalt {

Uint32 engine::fps = 60;
SDL_Renderer* engine::renderer;
SDL_Window* engine::window;
const Uint8* engine::keyboard;
Uint8* engine::keyboard_old;
int engine::mouse_x = 0;
int engine::mouse_y = 0;
int engine::numkeys;
bool engine::vsync = true; //keep it off for now, or users with different refresh rate will have the game run at different speeds!

engine::engine() {
	if (init()) { //error during init
		switch (MessageBox(NULL, L"Error initalizing game. Press abort to terminate program now, retry to try to initalize the game again, or ignore to try running the game anyways. It is recommended to retry, and to abort if the problem persists. Ignoring the error will probably just crash or hang the program, but you may try it as a last resort.", NULL, MB_ICONERROR | MB_ABORTRETRYIGNORE)) {
		case IDABORT: abort(); break;
		case IDRETRY: exit(); init(); break;
		case IDIGNORE: runloop(); break;
		}
	}
}

void engine::go() {
		if (!runloop()) {
			exit();
		}
		else { //abnormal termination during running
			if (MessageBox(NULL, L"A runtime error has occurred. It is recommended to terminate the program now by pressing abort. If you choose to continue, things might get a bit strange.", NULL, MB_ICONERROR | MB_ABORTRETRYIGNORE)==IDABORT) {
				abort();
			}
			runloop();
		}
}
engine::~engine() {
	
}

int engine::init()
{
	//SDL
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1){
		std::cout << SDL_GetError() << std::endl;
		return 1;
	}

	//Window
	SDL_Window *win = nullptr;
	win = SDL_CreateWindow("Hello World!", 100, 100, 1280, 720, SDL_WINDOW_SHOWN); //web statistics say that these days, over 99% people have a screen of this size or bigger, so... Yeah. That's it.
	exalt::engine::window=win;
	if (win == nullptr){
		std::cout << SDL_GetError() << std::endl;
		return 1;
	}

	//Renderer
	SDL_Renderer *ren = nullptr;
	if (engine::vsync) {
		ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_TARGETTEXTURE);
	} else {
		ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE);
	}
	exalt::engine::renderer=ren;
	if (ren == nullptr){
		std::cout << SDL_GetError() << std::endl;
		return 1;
	}

	//Keyboard
	exalt::engine::keyboard = SDL_GetKeyboardState(&numkeys);
	exalt::engine::keyboard_old = new Uint8[numkeys];

	//Audio
	int audio_rate = 44100;
	Uint16 audio_format = AUDIO_S16SYS;
	int audio_channels = 2;
	int audio_buffers = 4096;
	if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers) != 0) {
		fprintf(stderr, "Unable to initialize audio: %s\n", Mix_GetError());
		return 1;
	}
	Mix_AllocateChannels(EXALT_MIXER_CHANNELS);
	//Mix_Music *music; 
	//music = Mix_LoadMUS("../sfx/bg.ogg"); 
	//if (music == NULL) { printf("Unable to load Ogg file: %s\n", Mix_GetError()); return 1; }
	//Mix_PlayMusic(music, 0);

	//Text
	if (TTF_Init() == -1) {
		printf("TTF_Init: %s\n", TTF_GetError());
		return 1;
	}

	//All ok!
	return 0;
}

int engine::runloop() {
	int retval = 0;
	bool running = true;
	int t = 0;
#ifdef EXALT_EXCEPTIONS
	try {
#endif
	while (running) {
		t = SDL_GetTicks();
		memcpy(keyboard_old, keyboard, numkeys);
		SDL_PumpEvents();
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				running = false;
			}
		}
		SDL_GetMouseState(&exalt::engine::mouse_x, &exalt::engine::mouse_y);
		SDL_RenderClear(exalt::engine::renderer);
		update();
		SDL_RenderPresent(exalt::engine::renderer);
		t = SDL_GetTicks()-t;					//When vsync is enabled, SDL will automatically limit FPS in SDL_RenderPresent, so if your monitor refresh rate is 60,
		if (((1000/60)-t)>0) {					//this part will usually do nothing. It will mostly kick in when the refresh rate is over 60 or vsync is not available
			SDL_Delay((1000/60)-t);				//and it will then sleep for the rest of the frame to keep the framerate stable. It's slightly less smooth than
		}										//the automatic delay, but heck, it works fine most of the time.
		if ((SDL_GetTicks()-t) < (1000 / exalt::engine::fps)) { 
			SDL_Delay( ( 1000 / exalt::engine::fps ) - SDL_GetTicks() ); 
		}
	}
#ifdef EXALT_EXCEPTIONS
	}
	catch( const std::exception &e) {
		MessageBoxA(NULL, "Exception occured!", e.what(), MB_ICONERROR);
	}
#endif
	return retval;
}
int engine::exit() {
	SDL_DestroyRenderer(exalt::engine::renderer);
	SDL_DestroyWindow(exalt::engine::window);
	Mix_Quit();
	TTF_Quit();
	SDL_Quit();
	return 0;
}

void engine::registerobject(object* i, int priority) {
	objectlist.insert(priority,i);
}

void engine::addstate() {
	//gamestate.push_front()
}

bool engine::getkey(int key) {
	return keyboard[key] != 0;
}

bool engine::getkeypressed(int key) {
	return (keyboard[key] && !keyboard_old[key]) != 0;
}

bool engine::getkeyreleased(int key) {
	return (!keyboard[key] && keyboard_old[key]) != 0;
}

void engine::dostep() {
	for (auto it : objectlist) {
		it.second->step();
	}
}

void engine::dodraw() {
	for (auto it : objectlist) {
		it.second->draw();
	}
}

void engine::update() {
	for (auto it : objectlist) {
		it.second->update();
	}

	//for (auto it : gamestate) {
	//	it.draw();
	//}
	//gamestate.back().step();
}

point::point(int xx, int yy) {
	x = xx;
	y = yy;
}

}