#include <vector>
#include "exalt.h"
#include "draw.h"
#include "object.h"
#include <algorithm>

#pragma once
#ifndef EXALT_PARTICLES_
#define EXALT_PARTICLES_

namespace exalt {

struct particle {
	int x;
	int y;
	int w;
	int h;
	int lifetime;
	float angle;
	SDL_Texture* sprite;
	struct colors {
		int r, g, b, a;
	} blend;
	float xspeed;
	float yspeed;
	float xaccel;
	float yaccel;
	float anglespeed;
};

struct emitter {
	particle prototype;
};

struct particlesystem : public object {
public:
	particlesystem();
	~particlesystem();
	void step();
	void draw();
private:
	std::vector<particle> particles;
	std::vector<emitter> emitters;
};

}

#endif // EXALT_PARTICLES_