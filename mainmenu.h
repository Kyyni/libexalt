#pragma once
#ifndef EXALT_MAINMENU_
#define EXALT_MAINMENU_

#include "gamestate.h"

namespace exalt {

class mainmenu : public state {
public:
	mainmenu();
	~mainmenu();
	//void registerobject(object* i, int priority);
	virtual void step();
	virtual void draw() const;
private:
	//boost::ptr_multimap<int, object> objectlist;
	enum mainmenuoption {
		MMO_NEWGAME = 1,
		MMO_LOADGAME,
		MMO_OPTIONS,
		MMO_EXTRAS,
		MMO_QUIT
	} choice;
};

}
#endif //EXALT_MAINMENU_