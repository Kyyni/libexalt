#include "polyline.h"
#include "exalt.h"
#include "draw.h"
#include "geom.h"

namespace exalt {

void polyline::addpoint(exalt::point i) {
	line.push_back(i);
}

void polyline::draw() const {
	for (auto it = line.begin(), jt = --line.end(); it != jt; ++it) {
		draw_line(exalt::engine::renderer, *it, *(it+1));
	}
	draw_line(exalt::engine::renderer, geom::pointdir(geom::DEGREES_90 / 2.0f, 50, point(100,100)),
		geom::pointdir(geom::DEGREES_90 / 2.0f + geom::DEGREES_180, 50, point(100, 100)));
	draw_line(exalt::engine::renderer, geom::pointdir(geom::DEGREES_90 * 1.5f, 50, point(100, 100)),
		geom::pointdir(geom::DEGREES_90 * 1.5f + geom::DEGREES_180, 50, point(100, 100)));
}

}