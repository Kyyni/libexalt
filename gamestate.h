#pragma once
#ifndef EXALT_GAMESTATE_
#define EXALT_GAMESTATE_

#include "object.h"
#pragma warning(push)
#pragma warning( disable : 4512 )
#include <boost/ptr_container/ptr_map.hpp>
#pragma warning(pop)

namespace exalt {

class state : public object {
public:
	state();
	virtual ~state();
	//void registerobject(object* i, int priority);
	virtual void step();
	virtual void draw() const;
private:
	//boost::ptr_multimap<int, object> objectlist;
};

}
#endif //EXALT_GAMESTATE_