#include "character.h"
#include <math.h>

namespace exalt {

bool playerchar::place_meeting(int xx, int yy, bool onlystatic) {
	int xxx=mask.x, yyy=mask.y;
	bool r;
	mask.x=xx;
	mask.y=yy;
	if (onlystatic) {
		r=(world->collision(mask));
	} else {
		r=(world->collision(mask));
	}
	mask.x=xxx;
	mask.y=yyy;
	return r;
}

playerchar::~playerchar() {
//SHOULD NOT HAPPEN
	//assert(false);
}

playerchar::playerchar(int xx, int yy, int ww, int hh, SDL_Texture* ss) {
	x=xx;
	y=yy;
	w=ww;
	h=hh;
	mask.x=x;
	mask.y=y;
	mask.w=w;
	mask.h=h;
	mask.r=0;
	sprite=ss;
}

void playerchar::step() {
	if ((exalt::engine::keyboard[SDL_SCANCODE_A] == 1) || (exalt::engine::keyboard[SDL_SCANCODE_LEFT] == 1)) {
		if (!place_meeting(mask.x - walkspeed, mask.y)) { mask.x -= walkspeed; }
	}
	if ((exalt::engine::keyboard[SDL_SCANCODE_D] == 1) || (exalt::engine::keyboard[SDL_SCANCODE_RIGHT] == 1)) {
		if (!place_meeting(mask.x + walkspeed, mask.y)) { mask.x += walkspeed; }
	}
	if ((exalt::engine::keyboard[SDL_SCANCODE_W] == 1) || (exalt::engine::keyboard[SDL_SCANCODE_UP] == 1)) {
		if (!place_meeting(mask.x, mask.y - walkspeed)) { mask.y -= walkspeed; }
	}
	if ((exalt::engine::keyboard[SDL_SCANCODE_S] == 1) || (exalt::engine::keyboard[SDL_SCANCODE_DOWN] == 1)) {
		if (!place_meeting(mask.x, mask.y + walkspeed)) { mask.y += walkspeed; }
	}

	y=mask.y;
	x=mask.x;
}

}