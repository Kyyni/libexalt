#pragma once
#ifndef EXALT_POLYLINE_
#define EXALT_POLYLINE_

#include <vector>
#include "exalt.h"
#include "object.h"

namespace exalt {

class polyline : public object {
public:
	void addpoint(exalt::point);
	void empty();
	void draw() const override;
private:
	std::vector<exalt::point> line;
};


}

#endif // EXALT_POLYLINE_