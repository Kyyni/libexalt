#include "particles.h"
#include <math.h>

namespace exalt {

template< class ForwardIt, class T >
ForwardIt remove(ForwardIt first, ForwardIt last, const T& value)
{
	first = std::find(first, last, value);
	if (first != last)
		for(ForwardIt i = first; ++i != last; )
			if (!(*i == value))
				*first++ = std::move(*i);
	return first;
}

void particlesystem::draw() {
	std::for_each(particles.begin(), particles.end(), [](particle &p) {
		SDL_Point point;
		point.x=p.w/2;
		point.y=p.h/2;
		DrawSprite(exalt::engine::renderer,p.sprite,p.x,p.y,p.w,p.h,p.angle,SDL_FLIP_NONE,&point);
	});
}

void particlesystem::step() {
	std::for_each(particles.begin(), particles.end(), [this](particle &p){
		p.xspeed+=p.xaccel;
		p.yspeed+=p.yaccel;
		p.x+=static_cast<int>(round(p.xspeed));
		p.y+=static_cast<int>(round(p.yspeed));
		p.angle+=p.anglespeed;
		if (p.lifetime-- < 1) { 
			
		}
	});
	std::for_each(emitters.begin(), emitters.end(), [this](emitter &e) {
		this->particles.emplace_back(e.prototype);
	});
}

}