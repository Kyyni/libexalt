#include "collision.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>


namespace exalt {

bool rectangle::collision(const rectangle &o) {
	if ((r==0)&&(o.r==0)) {
		if( x+w <= o.x || x >= o.x+o.w ) return false;
		if( y+h <= o.y || y >= o.y+o.h ) return false;
		return true;
	} else {
		return isCollision(x+(w/2),y+(h/2),h/2,w/2,r,o.x+(o.w/2),o.y+(o.h/2),o.h/2,o.w/2,o.r);
	}
}

void collisionmesh::add(rectangle x) {
	mesh.push_back(x);
}

void collisionmesh::clear() {
	mesh.clear();
}

bool collisionmesh::collision(const rectangle &o) {
	for(auto it = mesh.begin(); it != mesh.end(); ++it) {
		if (it->collision(o)) {return true;}
	}
	return false;
}


namespace{
bool isCollision(int x10, int y10, int width1, int height1, float radrot1, int x20, int y20, int width2, int height2, float radrot2 ) { 
//thanks go to seymourandrew from
//forums.coronalabs.com/topic/39094-code-for-rotated-rectangle-collision-detection/
//ported to C++ by Kaj "Kake" Koivunen

//Yeah, I should have just studied the SAT theory and written my own, but making
//this valid C++ was a challenge in its own right.
	
//x10, y10 is centre point of rect1. x20, y20 is centre point of rect2
//height1, width1 are half heights/widths of rect1, radrot is rotation of rect in radians


	//TODO: Decrement all array indices by one to eliminate the extraneous indice 0. This is due to a temporary hacky workaround to a stack corruption bug yadda yadda.

	float radius1 = std::sqrt(static_cast<float>(( height1*height1 + width1*width1 )));
	float radius2 = std::sqrt(static_cast<float>(( height2*height2 + width2*width2 )));

	float angle1 = std::asin( height1 / radius1 );
	float angle2 = std::asin( height2 / radius2 );

	float x1[5] = {0}; 
	float y1[5] = {0};
	float x2[5] = {0}; 
	float y2[5] = {0};

	x1[1] = x10 + radius1 * std::cos(radrot1 - angle1); y1[1] = y10 + radius1 * std::sin(radrot1 - angle1);
	x1[2] = x10 + radius1 * std::cos(radrot1 + angle1); y1[2] = y10 + radius1 * std::sin(radrot1 + angle1);
	x1[3] = x10 + radius1 * std::cos(radrot1 + static_cast<float>(M_PI) - angle1); y1[3] = y10 + radius1 * std::sin(radrot1 + static_cast<float>(M_PI) - angle1);
	x1[4] = x10 + radius1 * std::cos(radrot1 + static_cast<float>(M_PI) + angle1); y1[4] = y10 + radius1 * std::sin(radrot1 + static_cast<float>(M_PI) + angle1);

	x2[1] = x20 + radius2 * std::cos(radrot2 - angle2); 
	y2[1] = y20 + radius2 * std::sin(radrot2 - angle2);
	x2[2] = x20 + radius2 * std::cos(radrot2 + angle2); 
	y2[2] = y20 + radius2 * std::sin(radrot2 + angle2);
	x2[3] = x20 + radius2 * std::cos(radrot2 + static_cast<float>(M_PI) - angle2); 
	y2[3] = y20 + radius2 * std::sin(radrot2 + static_cast<float>(M_PI) - angle2);
	x2[4] = x20 + radius2 * std::cos(radrot2 + static_cast<float>(M_PI) + angle2); 
	y2[4] = y20 + radius2 * std::sin(radrot2 + static_cast<float>(M_PI) + angle2);

	float axisx[5] = {0}; 
	float axisy[5] = {0};

	axisx[1] = x1[1] - x1[2]; 
	axisy[1] = y1[1] - y1[2];
	axisx[2] = x1[3] - x1[2]; 
	axisy[2] = y1[3] - y1[2];

	axisx[3] = x2[1] - x2[2]; 
	axisy[3] = y2[1] - y2[2];
	axisx[4] = x2[3] - x2[2]; 
	axisy[4] = y2[3] - y2[2];

	for (int k = 1; k<5; k++) {

		float proj = x1[1] * axisx[k] + y1[1] * axisy[k];

		float minProj1 = proj;
		float maxProj1 = proj;

		for (int i = 2; i<5; i++) {
			proj = x1[i] * axisx[k] + y1[i] * axisy[k];

			if (proj < minProj1) {
				minProj1 = proj;
			}
			else if (proj > maxProj1) {
				maxProj1 = proj;
			} 
		}

		proj = x2[1] * axisx[k] + y2[1] * axisy[k];

		float minProj2 = proj;
		float maxProj2 = proj;

		for (int j = 2; j<5; j++) {
			proj = x2[j] * axisx[k] + y2[j] * axisy[k];

			if (proj < minProj2) {
				minProj2 = proj;
			}
			else if (proj > maxProj2) {
				maxProj2 = proj;
			}

		}

		if ((maxProj2 <= minProj1) || (maxProj1 <= minProj2)) {
			return false;
		}
	}

	return true;

} //isCollision(...);
} //unnamed namespace



}