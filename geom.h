#pragma once
#ifndef EXALT_GEOM_
#define EXALT_GEOM_

#include "exalt.h"
#include <math.h>

namespace exalt {

	int random_range(int min, int max);

	namespace geom {

		//Just to make it absolutely clear, everything is in radians. Deal with it, so does C++.

		const float DEGREES_90 = static_cast<float>(M_PI / 2);
		const float DEGREES_180 = static_cast<float>(M_PI);
		const float DEGREES_270 = static_cast<float>(M_PI * 1.5);
		const float DEGREES_360 = static_cast<float>(M_PI * 2);

		float point_direction(exalt::point a, exalt::point b);
		float angle_difference(float a, float b);
		exalt::point pointdir(float direction, float distance = 1.0F, exalt::point origin = exalt::point(0, 0));
}
}

#endif // EXALT_GEOM_