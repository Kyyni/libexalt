#pragma once
#ifndef EXALT_CHARACTER_
#define EXALT_CHARACTER_

#include "exalt.h"
#include "collision.h"
#include "object.h"

namespace exalt {

class playerchar : public exalt::objectdrawable {
public:
	playerchar(int x, int y, int w, int h, SDL_Texture* sprite);
	~playerchar();
	bool place_meeting(int x, int y, bool onlystatic = false);
	void step() override;
	exalt::rectangle mask;
	exalt::collisionmesh* world;
	static const int walkspeed=2;
};

}

#endif // EXALT_CHARACTER_