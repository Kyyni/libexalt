#include "geom.h"
#include "exalt.h"

namespace exalt {


	int random_range(int min, int max) {
		return (min + (rand() % (int)(max - min + 1)));
	}

	namespace geom {

		//Just to make it absolutely clear, everything is in radians. Deal with it, so does C++.

		float point_direction(exalt::point a, exalt::point b) {
			return static_cast<float>(atan2(a.y - b.y, a.x - b.x));
		}

		float angle_difference(float a, float b) {
			return atan2(sin(a - b), cos(a - b));
		}

		exalt::point pointdir(float direction, float distance, exalt::point origin) {
			//literally the shitties function name ever. What should it be called?
			return exalt::point(static_cast<int>(origin.x + distance*cos(direction)),
				static_cast<int>(origin.y + distance*sin(direction)));
		}
	}
}