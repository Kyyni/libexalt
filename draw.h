#pragma once
#ifndef EXALT_SPRITE_
#define EXALT_SPRITE_

#include <iostream>
#include "exalt.h"
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

namespace exalt {

struct area{
	area(int xcoord=0, int ycoord=0, int width=0, int height=0, float rotation=0);
	int x;
	int y;
	int w;
	int h;
	float r;
};

SDL_Texture* LoadTexture(char file[], SDL_Renderer*, Uint8 alpha=255u);
TTF_Font* LoadFont(char file[], int size);

void DrawSprite(SDL_Renderer* ren, SDL_Texture* tex, int x, int y, int w, int h, double rot=0, SDL_RendererFlip flip=SDL_FLIP_NONE, SDL_Point* center=NULL);
void DrawSprite(SDL_Renderer* ren, SDL_Texture* tex, area pos=area(), area view=area(), SDL_RendererFlip flip=SDL_FLIP_NONE, SDL_Point* center=NULL);
void draw_line(SDL_Renderer* ren, point a, point b);
void draw_circle(SDL_Renderer* ren, point m, float r, int steps = 32);
void DrawText(SDL_Renderer* ren, TTF_Font* font, int x, int y, char text[], SDL_Color color);
}

#endif // EXALT_SPRITE_
