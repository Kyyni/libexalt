#include "draw.h"
#include "SDL_ttf.h"

namespace exalt {

area::area(int xcoord, int ycoord, int width, int height, float rotation) {
	x=xcoord;
	y=ycoord;
	w=width;
	h=height;
	r=rotation;
}

SDL_Texture* LoadTexture(char file[], SDL_Renderer *ren, Uint8 alpha) {
	SDL_Surface *bmp = nullptr;
	bmp = IMG_Load(file);
	if (bmp == nullptr){
		std::cout << SDL_GetError() << std::endl;
		return NULL;
	}
	SDL_Texture *tex = nullptr;
	tex = SDL_CreateTextureFromSurface(ren, bmp);
	SDL_FreeSurface(bmp);
	SDL_SetTextureAlphaMod(tex,alpha);
	return tex;
}

TTF_Font* LoadFont(char file[], int size) {
	return TTF_OpenFont(file, size);
}

void DrawSprite(SDL_Renderer* ren, SDL_Texture* tex, int x, int y, int w, int h, double rot, SDL_RendererFlip flip, SDL_Point* center) {
	SDL_Rect rect = {x,y,w,h};
	SDL_RenderCopyEx(ren, tex, NULL, &rect, rot, center, flip);
}

void DrawSprite(SDL_Renderer* ren, SDL_Texture* tex, area pos, area view, SDL_RendererFlip flip, SDL_Point* center) {
	SDL_Rect rect = {pos.x - view.x, pos.y - view.y, pos.w, pos.h};
	SDL_RenderCopyEx(ren, tex, NULL, &rect, pos.r, center, flip);
}

void draw_line(SDL_Renderer* ren, point a, point b) {
	SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
	SDL_RenderDrawLine(ren, a.x, a.y, b.x, b.y);
	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
}

void draw_circle(SDL_Renderer * ren, point m, float r, int steps) {
	point a(static_cast<int>(m.x+r), m.y), b(0, 0);
	double angle;
	for (int i = 1; i <= steps; i++) {
		angle = ((2 * M_PI) / steps)*i;
		b.y = static_cast<int>(std::sin(angle)*r + m.y);
		b.x = static_cast<int>(std::cos(angle)*r + m.x);
		draw_line(ren, a, b);
		a = b;
	}
}

void DrawText(SDL_Renderer* ren, TTF_Font* font, int x, int y, char text[], SDL_Color color) {
	SDL_Surface* text_surface = TTF_RenderText_Blended(font, text, color);
	SDL_Texture* text_texture = SDL_CreateTextureFromSurface(ren, text_surface);
	SDL_Rect rect = { x,y,text_surface->w,text_surface->h };
	SDL_RenderCopy(ren, text_texture, NULL, &rect);
	SDL_FreeSurface(text_surface);
	SDL_DestroyTexture(text_texture);
	//This is an understandably naive approach with no regards to performance.
	//For more permanently visible, high performance text, keep the texture
	//and re-use it.
}

}