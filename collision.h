#pragma once
#ifndef EXALT_COLLISION_
#define EXALT_COLLISION_

#include <vector>

namespace exalt {

struct rectangle {
	int x, y, w, h;
	float r;
	inline rectangle() {};
	inline rectangle(int xx, int yy, int ww, int hh, float rr=0) {x=xx; y=yy; w=ww; h=hh; r=rr;}
	bool collision(const rectangle &o);
};

struct line {
	int x1, x2, y1, y2;
};

struct circle {
	int x, y, r;
};

class collisionmesh {
public:
	void add(rectangle);
	void add(line);
	void add(circle);
	bool collision(const rectangle &o);
	void clear();
	//void removerectangle(int, int, int, int);
	//void removerectangle(rectangle);
private:
	std::vector<rectangle> mesh;
};

namespace {
bool isCollision(int x10, int y10, int width1, int height1, float radrot1, int x20, int y20, int width2, int height2, float radrot2 );
}

bool collisionrectangle2rectangle(rectangle &a, rectangle &b);
}


#endif // EXALT_COLLISION_