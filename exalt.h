#pragma once
#ifndef EXALT_EXALT_
#define EXALT_EXALT_

#include <iostream>
#include <map>
#include <stack>
#include <algorithm>
#include "object.h"
#include "gamestate.h"
#include <SDL.h>
#include <boost/ptr_container/ptr_map.hpp>
#include <boost/ptr_container/ptr_deque.hpp>

namespace exalt {

class engine {
public:
	static Uint32 fps;
	static SDL_Renderer* renderer;
	static SDL_Window* window;
	static const Uint8* keyboard;
	static Uint8* keyboard_old;
	static int mouse_x;
	static int mouse_y;
	static bool vsync;
	engine();
	~engine();
	void go();
	int init();
	int runloop();
	int exit();
	void registerobject(object*, int priority = 0);
	void addstate();
	static bool getkey(int key);
	static bool getkeypressed(int key);
	static bool getkeyreleased(int key);
private:
	static int numkeys;
	void dostep();
	void dodraw();
	void update();
	boost::ptr_multimap<int, object> objectlist;
	boost::ptr_deque<state> gamestate;
};

struct point {
	point(int x, int y);
	int x, y;

};

}

#endif // EXALT_EXALT_