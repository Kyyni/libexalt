#include "sound.h"
#include <iostream>

namespace exalt {
sound::sound(){}

sound::sound(char * filename){
	data = Mix_LoadWAV(filename);
	if (data == nullptr) {
		fprintf(stderr, "Error loading sound: %s\n", Mix_GetError());
	}
}

sound::~sound(){
	if (data != nullptr) {
		Mix_FreeChunk(data);
	}
}

bool sound::load(char * filename){
	data = Mix_LoadWAV(filename);
	if (data == nullptr) {
		fprintf(stderr, "Error loading sound: %s\n", Mix_GetError());
		return false;
	}
	return true;
}

void sound::play(){
	channel = Mix_PlayChannel(-1, data, 0);
	if (channel == -1) {
		fprintf(stderr, "Error playing sound: %s\n", Mix_GetError());
	}
}

void sound::playloop(){
	channel = Mix_PlayChannel(-1, data, -1);
	if (channel == -1) {
		fprintf(stderr, "Error playing sound: %s\n", Mix_GetError());
	}
}

void sound::stop(){
	Mix_HaltChannel(channel);
}

}