#pragma once
#ifndef EXALT_SOUND_
#define EXALT_SOUND_

#include <SDL.h>
#include "SDL_mixer.h"

namespace exalt {

class sound {
public:
	sound();
	sound(char* filename);
	~sound();
	bool load(char* filename);
	 void play();
	 void playloop();
	 void stop();
private:
	Mix_Chunk* data = nullptr;
	int channel;
};

}

#endif // EXALT_SOUND: