#pragma once
#ifndef EXALT_OBJECT_
#define EXALT_OBJECT_

#include <SDL.h>

namespace exalt {

class object {
public:
	object();
	virtual ~object();
	virtual void step();
	virtual void draw() const;
	virtual void update();
	bool active = true;
	bool visible = true;
};

class objectdrawable : public object {
public:
	objectdrawable();
	objectdrawable(int x, int y, int w, int h, SDL_Texture* sprite);
	virtual ~objectdrawable();
	virtual void step();
	virtual void draw() const;
	virtual void update();
	bool active = true;
	bool visible = true;
protected:
	int x;
	int y;
	int h;
	int w;
	SDL_Texture* sprite;
};
}
#endif // EXALT_OBJECT_